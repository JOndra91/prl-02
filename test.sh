#!/bin/bash

set -e

declare -r name='clapba'
declare -r cppFile="$name.cpp"
declare -r executable="$name"
declare numbersFile='numbers'
declare customNumbers=false

randomBinary() {
    local -ir bitCount=$1; shift

    for i in $(seq 1 $bitCount); do
        printf "%d" $(( $RANDOM % 2 ))
    done

    printf "\n"
}

generateNumbers() {
    local -i bitCount=$1; shift
    local -r genNums=$1; shift

    if (( bitCount <= 0 )); then
        if [[ -f "$numbersFile" ]]; then
            bitCount=$(head --lines 1 "$numbersFile" | tr -d $'\n' | wc --chars)
        else
            bitCount=$(( 2**($RANDOM % 7) ))
        fi
    fi

    if $genNums || [[ ! -f "$numbersFile" ]]; then
        rm -f "$numbersFile"

        for i in {1..2}; do
            randomBinary $bitCount >> "$numbersFile"
        done
    fi

    echo "$bitCount"
}

cleanup() {
    rm -f "$executable"
}

main() {
    local -i bitCount=0

    local -a cxxFlags=();
    local genNums=false

    local arg;
    while (( $# )); do
        arg="$1"; shift

        case "$arg" in
            --generate)
                genNums=true
                ;;
            --numbers)
                numbersFile="$1"; shift
                ;;
            --debug)
                cxxFlags+=('-DDEBUG')
                ;;
            --timers)
                cxxFlags+=('-DTIMERS')
                ;;
            *)
                if (( ! bitCount )); then
                    bitCount="$arg"
                else
                    printf '%s: %s' "$arg" 'Unexpected argument' 1>&2
                    exit 1
                fi
                ;;
        esac
    done

    if (( bitCount <= 0 )) || [[ ! -f "$numbersFile" ]] || $genNums; then
        bitCount=$(generateNumbers $bitCount $genNums)
    fi

    local -i cpuCount=$(( 2*$bitCount - 1 ))

    local -a mpiFlags=()
    if [[ "`uname --nodename`" == 'merlin.fit.vutbr.cz' ]]; then
        mpiFlags+=(
            --prefix /usr/local/share/OpenMPI
        )
    fi

    trap cleanup EXIT

    export OMPI_CXXFLAGS="${cxxFlags[@]}"
    mpic++ "${mpiFlags[@]}" -o "$executable" "$cppFile"
    mpirun "${mpiFlags[@]}" -np $cpuCount "$executable" "$numbersFile" |
      sort --numeric-sort | grep --color=auto -E 'overflow|$'
}

main "$@"
