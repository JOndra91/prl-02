%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\documentclass[12pt,a4paper,notitlepage,final]{article}
% cestina a fonty
\usepackage[czech]{babel}
\usepackage[utf8]{inputenc}
\usepackage[T1]{fontenc}
% obrazky
\usepackage[dvipdf]{graphicx}
% velikost stranky
\usepackage[top=3.5cm, left=2.5cm, text={17cm, 24cm}, ignorefoot]{geometry}
%% cislovani objektu
\usepackage{chngcntr}
\counterwithin{figure}{section}
% balicky pro odkazy
\usepackage{color}
\usepackage{url}
\usepackage[unicode,colorlinks,hyperindex,plainpages=false,pdftex,linktoc=all]{hyperref}
\definecolor{urlBlue}{rgb}{0.1,0,0.7}
\definecolor{citeGreen}{rgb}{0,0.5,0}
\definecolor{linkRed}{rgb}{0.8,0.1,0.1}
\definecolor{fileSteelBlue}{rgb}{0,0.2,0.4}
\hypersetup{
    linkcolor=linkRed,          % color of internal links
    citecolor=citeGreen,        % color of links to bibliography
    filecolor=fileSteelBlue,    % color of file links
    urlcolor=urlBlue            % color of external links
}
\usepackage{color,colortbl}
\usepackage{multirow}

\usepackage{amsfonts}
\usepackage{amsmath}

\usepackage{pgfplots}
\usepackage{pgfplotstable}
\pgfplotsset{compat=1.10}

\definecolor{tableGray}{rgb}{0.8,0.8,0.8}
% mezera mezi odstavci
\setlength{\parskip}{0.5\baselineskip}%

\begin{document}

\begin{center}
  \begin{Large}
    Carry Look Ahead Parallel Binary Adder
  \end{Large}
\end{center}

\begin{center}
  Ondřej Janošík <xjanos12@stud.fit.vutbr.cz>
\end{center}

\section{Popis algoritmu}

Jedná se o~paralelní sčítačku, která pracuje nad vyváženým binárním stromem
(počet sčítaných bitů $n$ musí být mocninou 2).
Vstupem jsou dvě binární čísla $X = (x_{n-1}, \ldots, x_0)$
a~$Y = (y_{n-1}, \ldots, y_0)$.
Na výstupu se pak nachází výsledné binární číslo $Z = (z_{n-1}, \ldots, z_0)$.
Jednotlivé bity obou sčítaných hodnot se nacházejí v~odpovídajících listech.
Výpočet hodnoty pak probíhá podle vzorce \ref{eq:result}.

\begin{equation}
  \label{eq:result}
  z_i = x_i + y_i + c_i \in \mathbb{Z}_2
\end{equation}

Před výpočtem výsledné hodnoty je třeba spočítat všechny bity přenosu
$C = (c_{n-1}, \ldots, c_{0})$.
Listové uzly vypočítají hodnotu $D = (d_{n-1}, \ldots, d_{0})$ podle vzorce
\ref{eq:initial_d}. Tato hodnota indikuje možnosti přenosu:

\begin{itemize}
  \item Generate ($g$) - Přenos nastane.
  \item Propagate ($p$) - Přenos může nastat.
  \item Stop ($s$) - Přenos nenastane.
\end{itemize}

\begin{equation}
  d_i =
    \begin{cases}
      g, & x_i = 1 \land y_i = 1 \\
      s, & x_i = 0 \land y_i = 0 \\
      p, & \text{jinak}
    \end{cases}
  \label{eq:initial_d}
\end{equation}

Následně se spočíta prefixová suma s~asociativní operací $\odot$ pomocí
\textit{scan} algoritmu.
Poté lze spočítat výsledné bity přenosu pomocí rovnice
\ref{eq:carry_bit}. Bity přenosu jsou poté pomocí operace \textit{shift}
předány uzlu reprezentující významější bit,
přičemž nejméně významný bit má carry bit nastavený na 0.
Detekce přetečení výsledku pak provádíme pomocí nerovnice \ref{eq:overflow}.

\begin{equation}
  c_i =
    \begin{cases}
      1, & d_i = g \\
      0, & \text{jinak}
    \end{cases}
  \label{eq:carry_bit}
\end{equation}

\begin{equation}
  x_{n-1} + y_{n-1} + c_{n-1} > 1
  \label{eq:overflow}
\end{equation}

\begin{table}[h]
    \centering
    \begin{tabular}[c]{| c | c | c | c |}
        \hline
        $\odot$ & s & p & g \\ \hline
        s & s & s & s  \\ \hline
        p & s & p & g  \\ \hline
        g & g & g & g  \\ \hline
    \end{tabular}
    \caption{Definice operace $\odot$.}
    \label{tab:operator}
\end{table}

\subsection{Prescan}

Algoritmus \textit{scan} se skládá ze 4 částí:

\begin{itemize}
  \item Upsweep
  \item Downsweep
  \item Shift
  \item Reduce
\end{itemize}

Vstupem algoritmu je asociativní operátor ($\oplus$), neutrální prvek ($I$) a
vektor hodnot ($A = (a_0, \ldots, z_{n-1})$).
Výsledkem algoritmu je nový vektor
$A' = (a_0, a_0 \oplus a_1, \ldots, a_0 \oplus \ldots \oplus a_{n-1})$.

V~našem případě je asociativní operátor $\odot$, neutrální prvek $p$ a vstupní
vektor $D$. Na konci algoritmu se v~listových uzlech nacházejí hodnoty $D'$,
přičemž tyto hodnoty jsou uloženy zprava doleva (nejpravější list obsahuje
hodnotu $a_0$).

\subsubsection{Upsweep}

Hodnoty $d$ se propagují ve stromu z~potomků do rodičů, přičemž rodičovské uzly
si pamatují hodnoty svých potomků a hodnotu $d = l_d \odot r_d$, kde $l_d$
a $r_d$ jsou hodnoty $d$ levého a pravého potomka.
V~kořenovém uzlu pak dostaneme celkovou sumu ($d_0 \odot \ldots \odot d_{n-1}$).
Algoritmus je v~tomto podobný \textit{reduce}.

\subsubsection{Dowsweep}

Hodnotu $d$ kořenového uzlu nahradíme neutrálním prvkem
(v~tomto případě hodnotou $p$). Nové hodnoty $d$ se propagují stromem od rodičů
k~potomkům, přičemž hodnoty pro levého a pravého potomka jsou vypočítány pomocí
rovnic \ref{eq:child_d}, kde $l_d'$ a $r_d'$ jsou nové hodnoty zasílané potomkům.

\begin{equation}
  \begin{aligned}
    l_d' & = r_d \odot d \\
    r_d' & = d
  \end{aligned}
  \label{eq:child_d}
\end{equation}

\subsubsection{Shift \& Reduce}

Během algoritmu \textit{shift} se předávají hodnoty mezi sousedícími listovými
uzly směrem k~méně významnému bitu. Hodnota nejvýznamnějšího bit je pak
celkovou sumou vstupních hodnot algoritmu \textit{scan}. Ta již byla vypočtena
během \textit{upsweep} části.

\section{Složitost algoritmu}

Na začátku algoritmu jsou všechny uzly kromě listových prázdné.
Výpočet hodnoty $D$ lze provést paralelně v~jednom kroku ($1$).
Algoritmus \textit{scan} se skládá ze 4 částí. \textit{Upsweep} a
\textit{downsweep} potřebují $log_2(n)$ kroků. \textit{Shift} je konstatní
operace ($1$). Vzhledem k~tomu, že celkovou sumu již známe z~\textit{upsweep}
části, tak operace \textit{reduce} je rovněž konstatní ($1$).
V~opačném případě by se jednalo o~operaci logaritmickou ($log_2(n)$).
Výslednou hodnotu $Z$ lze pak opět paralelně v~jednom kroku ($1$).

Vyplývající časovou složitost popisuje rovnice \ref{eq:time_complexity}.
Nejvýznamnější složkou je $log_2(n)$ a proto se jedná o~algoritmus s~logaritmickou
časovou složitostí.

\begin{equation}
  t(n) = 4 + 2 \cdot log_2(n) = O(log_2(n))
  \label{eq:time_complexity}
\end{equation}

Pro sečtení $n$ bitových čísel potřebujeme $n$ listů ve vyváženém binárním
stromu. Z toho vyplývá prostorová složitost (\ref{eq:space_complexity}).
Dominantní složkou je $n$ a prostorová složitost je tedy linerání.

\begin{equation}
  s(n) = 2 \cdot n - 1 = O(n)
  \label{eq:space_complexity}
\end{equation}

Protože je každý uzel reprezentován právě jedním procesorem, je potřebný
počet procesorů dán počtem uzlů stejně jako v~případě prostorové složitosti
(\ref{eq:cpu_complexity}).

\begin{equation}
  p(n) = s(n) = O(n)
  \label{eq:cpu_complexity}
\end{equation}

Cena paraleního řešení je dána potřebným počtem procesorů a časovou složitostí
(\ref{eq:parallel_complexity}).

\begin{equation}
  c(n) = t(n) \cdot p(n) = O(n \cdot log_2(n))
  \label{eq:parallel_complexity}
\end{equation}

\section{Implementace}

Každý uzel je reprezentován objektem třídy \texttt{Node}, který je vždy
vytvořen na začátku procesu. Tato třída poskytuje užitečné jako jsou
metody pro rozlišení pozice uzlu (\texttt{isLeaf()}, \texttt{isRoot()})
a také odstraňuje nutnost použití globálních proměnných, či explicitního
předávání parametrů funkcím.

\begin{figure}
  \centering
  \includegraphics[width=12cm,keepaspectratio]{img/communication_diagram}
  \caption{Komunikační diagram.}
  \label{fig:communication_diagram}
\end{figure}

Na začátku přečte kořenový uzel hodnoty v~souboru a rozešle je všem listovým
uzlům. Pokud délka hodnot v~souboru neodpovídá požadované délce,
jsou odebrány či doplněny (nulami) nejvýznamější bity.

Listové uzly následně v~metodě \texttt{computeInitialD()} vypočítají počáteční
hodnotu \texttt{valueD} ($d$).

Následuje volání metody \texttt{upSweep()}, během které všechny uzly příjmou
hodnotu \texttt{valueD} svých potomků, vypočítají svou hodnotu \texttt{valueD}
a tu zašlou svému rodičovskému uzlu.

Na začátku další metody \texttt{downSweep()} se kořenovému uzlu přířadí hodnota
\texttt{valueD = D\_PROPAGATE} (neutrální prvek).
Každý uzel pak příjme novou hodnotu $d$ od svého rodiče a zašle levému
potomkovi hodnotu \texttt{dProduct(rightD, valueD)} a pravému potomkovi
hodnotu \texttt{valueD}.

Pomocí \texttt{shiftAndReduce()} se dokončí operace \textit{scan} a~následně
se pomocí \texttt{sendCarry()} zašlou bity přenosu sousedícím listům směrem
k~významnějšímu bitu.

Nyní již lze spočítat výsledek pomocí metody \texttt{getResult()}, který
je společně s~id procesoru vypsán.

Nakonec je pomocí dvojice metod \texttt{isMSB()} (určuje zda se jedná
o~nejvýznamnější bit) a \texttt{overflows()}
detekováno případné přetečení na nejvýznamějším bitu.

Algoritmus končí ve chvíli, kdy všechny listy vypočítají výslednou hodnotu
a list reprezentující nejvýznamnější bit zjistí příznak přetečení.

\section{Měření}

Pro měření jsem použil bashový script \textit{measure.sh}, který v~nekonečném
cyklu spouští program \textit{mes} s~počtem řazených hodnot
$2^n$ kde $n=\{1,\ldots,7\}$.
Výsledky jednotlivých měření ukládá do složky s~názvem
\textit{results\_<datum+čas spuštění>} a při přerušení skriptu je ve složce
vygenerován soubor \textit{results} obsahující průměr a medián pro jednotlivé
varianty měření. Pro jednotlivé počty sčítaných bitů bylo naměřeno přibližně
1800 hodnot.

Samotné měření času je realizováno pomocí funkce \texttt{clock\_gettime()}
z~hlavičkového souboru \texttt{<ctime.h>}.
Měření lze zapnout skriptu \textit{test.sh} s~parametrem \textit{-{}-timers},
případně nastavením proměnné prostředí na \texttt{OMPI\_CXXFLAGS='-DTIMERS'}.

\begin{figure}[h]
    \centering
    \begin{tikzpicture}
    \begin{axis}[
        width=\textwidth,
        height=6cm,
        legend pos=north west,
        ylabel={Čas [ms]},
        xlabel={Počet sčítaných bitů},
        log basis x={2},
        xmode=log,
        ymode=log,
        log ticks with fixed point
      ]

    \addplot[mark=none, blue] table[x=count, y=average, col sep=comma]{plot/results};
    \addplot[mark=none, red] table[x=count, y=median, col sep=comma]{plot/results};
    \addplot[mark=none, black, densely dotted] table[x=count, y expr=ln(\thisrow{count})/ln(2)*0.4, col sep=comma]{plot/results};

    \legend{Průměr, Medián, Předpokládaný průběh}
    \end{axis}
    \end{tikzpicture}
    \caption{Graf rychlosti sčítání v~závislosti na počtu sčítaných bitů (logaritmická časová osa).}
    \label{plot:experiments_log_y}
\end{figure}

\begin{figure}[h]
    \centering
    \begin{tikzpicture}
    \begin{axis}[
        width=\textwidth,
        height=6cm,
        legend pos=north west,
        ylabel={Čas [ms]},
        xlabel={Počet sčítaných bitů},
        log basis x={2},
        xmode=log,
        log ticks with fixed point
      ]

    \addplot[mark=none, blue] table[x=count, y=average, col sep=comma]{plot/results};
    \addplot[mark=none, red] table[x=count, y=median, col sep=comma]{plot/results};
    \addplot[mark=none, black, densely dotted] table[x=count, y expr=ln(\thisrow{count})/ln(2)*0.4, col sep=comma]{plot/results};

    \legend{Průměr, Medián, Předpokládaný průběh}
    \end{axis}
    \end{tikzpicture}
    \caption{Graf rychlosti sčítání v~závislosti na počtu sčítaných bitů (linerání časová osa).}
    \label{plot:experiments_lin_y}
\end{figure}

Z~grafů je patrné, že předpokládaná časová složitost nebyla potvrzena.
Předpokládaný průběh není nijak vědecky získaný ($log_2(n) \cdot 0.4$),
jedná se spíše o~pomocnou křivku pro ověření průběhu logaritmické křivky
v~grafu.

\section{Závěr}

Naměřené hodnoty neodpovídají teoretické časové složitosti.
To může být způsobeno nedostatečným hardwarem. Počet fyzických procesorů
je menší než požadovaný počet procesorů a režie přepínání kontextu CPU jádra
není v~tomto případě úplně zanedbatelná. To se zejména projevuje při vyšším
počtu použitých procesorů.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% \newpage
%
% \bibliographystyle{czechiso}
% \begin{flushleft}
% \bibliography{zdroje} % viz. literatura.bib
% \end{flushleft}

\end{document}
