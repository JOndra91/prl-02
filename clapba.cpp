/*
 * File:   clapba.cpp
 * Author: Ondřej Janošík <xjanos12@stud.fit.vutbr.cz>
 */

#include <mpi.h>
#include <iostream>
#include <fstream>

using namespace std;

#ifdef TIMERS
#include <ctime>
#endif

#define TAG_DISTRIBUTE 0
#define TAG_DOWNSWEEP 1
#define TAG_UPSWEEP 2
#define TAG_SHIFT_REDUCE 3
#define TAG_CARRY 4

#ifdef DEBUG
#define dprintf(args...) do{fprintf(stderr, args); fflush(stderr);} while(0)
#else
#define dprintf(...)
#endif

struct ab_t {
    int a;
    int b;
};

enum d_t {
    D_PROPAGATE = 0,
    D_STOP = 1,
    D_GENERATE = 2,
    D_INVALID = 3,
};

const char *d_tMap[4] = {
  [0] = "D_PROPAGATE",
  [1] = "D_STOP",
  [2] = "D_GENERATE",
  [3] = "D_INVALID",
};

class Node {

private:
    int nodeCount, bitCount;
    int nodeId, leftId, rightId, parentId;
    d_t valueD, leftD, rightD, reduceD;
    ab_t operands;
    int bitId;

    bool bIsRoot;
    bool bIsLeaf;
    int carry;

    MPI_Status status;

public:
    Node(int _nodeId, int _nodeCount) {
        nodeCount = _nodeCount;
        nodeId = _nodeId;
        bitCount = (nodeCount + 1) >> 1;
        bitId = nodeCount - nodeId - 1;

        bIsRoot = nodeId == 0;
        bIsLeaf = bitId < bitCount;

        carry = 0;

        if(bIsLeaf) {
            leftId = -1;
            rightId = -1;
        }
        else {
            leftId = (nodeId << 1) + 1;
            rightId = (nodeId << 1) + 2;
        }

        parentId = (nodeId - 1) >> 1;
    }

    inline bool isRoot() {
        return bIsRoot;
    }

    inline bool isLeaf() {
        return bIsLeaf;
    }

    inline int getNodeId() {
        return nodeId;
    }

    inline bool isMSB() {
        return bitId == bitCount - 1;
    }

    inline bool isLSB() {
      return bitId == 0;
    }

    inline bool getResult() {
        return getSum() % 2;
    }

    inline int getSum() {
        return operands.a + operands.b + carry;
    }

    inline bool overflows() {
        return getSum() > 1;
    }

    void distributeNumbers(string numbersFiles) {
        ifstream numbers;
        string a,b;

        numbers.open(numbersFiles.c_str(), ios::binary);

        dprintf("Bit count is %d\n", bitCount);

        numbers >> a;
        numbers >> b;

        a = fixNumber(a);
        b = fixNumber(b);

        ab_t ab;
        int bitOffset = bitCount - 1;
        for(int bit = 0; bit < bitCount; ++bit) {
            int strIdx = bitOffset - bit;
            int dstNodeId = bitOffset + strIdx;

            ab.a = a[strIdx] - '0';
            ab.b = b[strIdx] - '0';

            dprintf("Sending tuple (%d, %d) to node %d\n", ab.a, ab.b, dstNodeId);
            MPI_Send(&ab, 2, MPI_INT, dstNodeId, TAG_DISTRIBUTE, MPI_COMM_WORLD);
        }

        numbers.close();
    }

    /**
     * Add left zero padding or removes characters from left in order to
     * get bit string of right length.
     */
    string fixNumber(string n) {
        int diff = n.length() - bitCount;

        if(diff < 0) {
            return string(-diff, '0').append(n);
        }
        else {
          return n.substr(diff);
        }
    }

    void recieveDistributedNumber() {
        MPI_Recv(&operands, 2, MPI_INT, 0, TAG_DISTRIBUTE, MPI_COMM_WORLD, &status);
        dprintf("Node %d received tuple (%d, %d)\n", nodeId, operands.a, operands.b);
    }

    /**
     * Computes product of two d_t values according to following table:
     *
     *  * | s p g
     *  --+------
     *  s | s s s  # s - STOP
     *  p | s p g  # p - PROPAGATE
     *  g | g g g  # g - GENERATE
     *
     * Formula is based on binary values of constants.
     */
    d_t dProduct(d_t a, d_t b) {
        d_t r = (d_t)(a | b);

        return r == D_INVALID ? a : r;
    }

    void computeInitialD() {
        valueD = (d_t)((operands.a == operands.b) << operands.a);

        dprintf("Node %d computed initial D value: '%s' from (%d, %d)\n", nodeId, d_tMap[valueD], operands.a, operands.b);
    }

    void upSweep() {
        if(!isLeaf()) {
            MPI_Recv(&leftD, 1, MPI_INT, leftId, TAG_UPSWEEP, MPI_COMM_WORLD, &status);
            MPI_Recv(&rightD, 1, MPI_INT, rightId, TAG_UPSWEEP, MPI_COMM_WORLD, &status);

            dprintf("Node %d received leftD %d:'%s' and rightD %d:'%s' (upsweep).\n", nodeId, leftId, d_tMap[leftD], rightId, d_tMap[rightD]);

            // Store reduce for later use
            reduceD = valueD = dProduct(leftD, rightD);
            dprintf("Node %d computed new D value: '%s' from (%s, %s)\n", nodeId, d_tMap[valueD], d_tMap[leftD], d_tMap[rightD]);
        }

        if(!isRoot()) {
            MPI_Send(&valueD, 1, MPI_INT, parentId, TAG_UPSWEEP, MPI_COMM_WORLD);
            dprintf("Node %d send D value '%s' to parent node %d (upsweep).\n", nodeId, d_tMap[valueD], parentId);
        }
    }

    void downSweep() {
        if(isRoot()) {
            valueD = D_PROPAGATE;
            dprintf("Node %d set D value to '%s' (downsweep).\n", nodeId, d_tMap[valueD]);
        }
        else {
            MPI_Recv(&valueD, 1, MPI_INT, parentId, TAG_DOWNSWEEP, MPI_COMM_WORLD, &status);
            dprintf("Node %d received D value '%s' from parent node %d (downsweep).\n", nodeId, d_tMap[valueD], parentId);
        }

        if(!isLeaf()) {
            leftD = dProduct(rightD, valueD);
            rightD = valueD;

            MPI_Send(&leftD, 1, MPI_INT, leftId, TAG_DOWNSWEEP, MPI_COMM_WORLD);
            MPI_Send(&rightD, 1, MPI_INT, rightId, TAG_DOWNSWEEP, MPI_COMM_WORLD);

            dprintf("Node %d send leftD %d:'%s' and rightD %d:'%s' (downsweep).\n", nodeId, leftId, d_tMap[leftD], rightId, d_tMap[rightD]);
        }
    }

    void shiftAndReduce() {
      if(isLeaf()) {
        if(!isLSB()) {
          MPI_Send(&valueD, 1, MPI_INT, nodeId + 1, TAG_SHIFT_REDUCE, MPI_COMM_WORLD);
          dprintf("Node %d send D value '%s' to right node %d (shift & reduce).\n", nodeId, d_tMap[valueD], nodeId + 1);
        }

        MPI_Recv(&valueD, 1, MPI_INT, MPI_ANY_SOURCE, TAG_SHIFT_REDUCE, MPI_COMM_WORLD, &status);
        dprintf("Node %d received D value '%s' from left node %d (shift & reduce).\n", nodeId, d_tMap[valueD], status.MPI_SOURCE);
      }
      else if(isRoot()) {
        // Reduce was already calculated during upsweep
        dprintf("Reduce D is %d\n", reduceD);
        MPI_Send(&reduceD, 1, MPI_INT, bitCount - 1, TAG_SHIFT_REDUCE, MPI_COMM_WORLD);
        dprintf("Root %d send D value '%s' to MSB node %d (shift & reduce).\n", nodeId, d_tMap[reduceD], bitCount - 1);
      }
    }

    void sendCarry() {
      if(isLeaf()) {
        int lCarry = valueD == D_GENERATE;
        if(!isMSB()) {
          MPI_Send(&lCarry, 1, MPI_INT, nodeId - 1, TAG_CARRY, MPI_COMM_WORLD);
          dprintf("Node %d send carry (%d) to left node %d (send carry).\n", nodeId, lCarry, nodeId - 1);
        }

        if(!isLSB()) {
          MPI_Recv(&carry, 1, MPI_INT, MPI_ANY_SOURCE, TAG_CARRY, MPI_COMM_WORLD, &status);
          dprintf("Node %d received carry (%d) from right node %d (send carry).\n", nodeId, carry, status.MPI_SOURCE);
        }
      }
    }

};

int main(int argc, char** argv) {

    int nodeCount;
    int nodeId;
    string numbersFile;

    MPI_Init(&argc,&argv);
    MPI_Comm_size(MPI_COMM_WORLD,&nodeCount);
    MPI_Comm_rank(MPI_COMM_WORLD,&nodeId);

    Node n(nodeId, nodeCount);

    if(n.isRoot()) {
        if(argc >= 2) {
            numbersFile = string(argv[1]);
        }
        else {
            dprintf("Error: %s\n", "Expecting numbers file name as argument.");
            MPI_Abort(MPI_COMM_WORLD, 1);
        }
        n.distributeNumbers(numbersFile);
    }

    if(n.isLeaf()) {
        // All leaf nodes receive distributed numbers.
        n.recieveDistributedNumber();
    }

#ifdef TIMERS
    struct timespec start;

    MPI_Barrier(MPI_COMM_WORLD);

    if(n.isRoot()) {
        clock_gettime(CLOCK_MONOTONIC, &start);
    }
#endif

    if(nodeCount > 1) {
      if(n.isLeaf()) {
          n.computeInitialD();
      }

      n.upSweep();
      n.downSweep();

      // Three more unnecessary operations just to use scan. Well prescan is
      // enough, but noooo, someone tries to be pedantic and requires scan.
      n.shiftAndReduce(); // https://www.youtube.com/watch?v=h2Sc8QVtGic
      n.sendCarry();      // https://www.youtube.com/watch?v=c03Y0Ev99LQ
    }

    int result;
    if(n.isLeaf()) {
        result = n.getResult();
    }

#ifdef TIMERS
    struct timespec end, diff;
    double ddiff;

    MPI_Barrier(MPI_COMM_WORLD);

    if(n.isRoot()) {
        clock_gettime(CLOCK_MONOTONIC, &end);

        diff.tv_sec = end.tv_sec - start.tv_sec;
        diff.tv_nsec = end.tv_nsec - start.tv_nsec;

        if(diff.tv_nsec < 0) {
            diff.tv_sec -= 1;
            diff.tv_nsec += 1000000000;
        }

        // Miliseconds
        ddiff = ((diff.tv_sec * 1e3f) + (diff.tv_nsec / 1e6f));

        fprintf(stderr, "%f\n", ddiff);
    }
#endif

    if(n.isLeaf()) {
        printf("%d:%d\n", n.getNodeId(), result);
        dprintf("/%d:%d\n", n.getNodeId(), n.getSum());
    }

    if(n.isMSB() && n.overflows()) {
        printf("overflow\n");
    }

    dprintf("Node %d has left.\n", n.getNodeId());

    MPI_Finalize();

    return 0;
}
