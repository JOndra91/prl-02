#!/usr/bin/env runhaskell

import Control.Monad
import Data.Char
import Data.List
import Data.Maybe
import Data.Monoid
import Numeric
import System.Environment
import System.Exit
import System.IO
import Text.Read
import Data.Bool

main :: IO ()
main = do
    input <- getContents
    let tuples = mapMaybe readInputLine $ lines input
        sorted = sortOn fst tuples
        overflows = elem "overflow" $ lines input
        bResult = bool '0' '1' overflows : (snd <$> sorted)
        result = fromMaybe 0 $ readBin bResult :: Integer
    bs@[b1,b2] <- take 2 . lines <$> (readFile . head =<< getArgs)
    let vs@[v1, v2] = mapMaybe readBin bs :: [Integer]
        expected' = sum vs
    hPutStrLn stderr $ "Input:    " <> b1 <> " (" <> show v1 <> ")"
    hPutStrLn stderr $ "Input:    " <> b2 <> " (" <> show v2 <> ")"
    let bExpected' = replicate (length bResult) '0' <> showBin expected'
        bExpected = reverse . take (length bResult) $ reverse bExpected'
        expected = fromMaybe 0 $ readBin bExpected
    hPutStrLn stderr $ "Expected:  " <> bExpected <> " (" <> show expected <> ")"
    hPutStrLn stderr $ "Result:    " <> bResult <> " (" <> show result <> ")"
    putStr input
    unless (result == expected)
        exitFailure
  where
    readInputLine :: String -> Maybe (Int, Char)
    readInputLine ln = (\i -> (i, inputValue ln)) <$> readMaybe (inputInt ln)

    inputInt = takeWhile (/= ':')
    inputValue = (!! 1) . dropWhile (/= ':')

readBin :: Integral a => String -> Maybe a
readBin = fmap fst . listToMaybe . readInt 2 (`elem` "01") digitToInt

showBin :: (Integral a, Show a) => a -> String
showBin dec = showIntAtBase 2 intToDigit dec ""
