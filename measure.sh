#!/bin/bash

set -e

declare -r resultsDir="results_`date +'%F-%T'`"
declare -r name='clapba'
declare -r cppFile="$name.cpp"
declare -r executable="$name"
declare numbersFile='measure-numbers'

randomBinary() {
    local -ir bitCount=$1; shift

    for i in $(seq 1 $bitCount); do
        printf "%d" $(( $RANDOM % 2 ))
    done

    printf "\n"
}

generateNumbers() {
    local -i bitCount=$1; shift
    local -r genNums=$1; shift

    if (( bitCount <= 0 )); then
        if [[ -f "$numbersFile" ]]; then
            bitCount=$(head --lines 1 "$numbersFile" | tr -d $'\n' | wc --chars)
        else
            bitCount=$(( 2**$($RANDOM % 7) ))
        fi
    fi

    if $genNums || [[ ! -f "$numbersFile" ]]; then
        rm -f "$numbersFile"

        for i in {1..2}; do
            randomBinary $bitCount >> "$numbersFile"
        done
    fi

    echo "$bitCount"
}

finish() {

    echo "Collecting results"

    printf 'count,median,average\n' > "$resultsDir/results"

    for f in "$resultsDir"/measure_*; do

        local -i num=`sed -r 's/^.*\/measure_0*//' <<< "$f"`

        local average="`./average.hs "$f"`"
        local median="`./median.hs "$f"`"

        printf '%d,%s,%s\n' \
            $num $median $average >> "$resultsDir/results"
    done

    echo "Done"

    rm -f "$numbersFile"
    rm -f "$executable"
}

main() {
    trap finish EXIT

    export OMPI_CXXFLAGS='-DTIMERS'
    mpic++ -o "$executable" "$cppFile"

    mkdir "$resultsDir"

    ulimit -n 4096

    while true; do
        for i in {1..7}; do

            local -i bitCount=$((2**$i))
            local -i cpuCount=$(( 2*$bitCount - 1 ))

            local suffix=`printf '%03d' $bitCount`

            printf 'Running with %d values and %d processors.\n' $bitCount $cpuCount

            generateNumbers $bitCount true > /dev/null
            mpirun -np $cpuCount "$executable" "$numbersFile" \
                2>> "${resultsDir}/measure_${suffix}" > /dev/null
        done
    done

}

main "$@"
